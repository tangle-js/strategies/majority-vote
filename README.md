# @tangle/majority-vote

see tests for examples.

## API

### `Overwrite(opts) => overwrite`

opts *Object* (optional) can have properties:
- `identity`
    - define the identity transformation for this strategy
    - default `{}`
- `reifiedIdentiy`
    - define with `reify(identity)` resolves to
    - default `null`,
- `valueSchema` *Object*
    - add a JSON-schema to validate the values which are allowed to be passed in
    - default 'array'

### `overwrite.schema`
### `overwrite.isValid`
### `overwrite.identity() => I`
### `overwrite.concat() => I`
Majority votes concatenates votes together in an array.
votes:['hello', 'world'] + votes:['hello', 4] = votes: ['hello', 'world', 'hello', 4]

Note: complex objects are not supported currently:
{ vote: [{ content: 'dog' }, { content: 'cat', test: true }, { test: true, content: 'cat' }] } does not give expected result.

### `overwrite.humanToTransform(currentT, input) => T`
Currently the input should be an array, but it may be easier for humans to just give the value they're voting for.

### `overwrite.reify(T) => t`
Reify turns the votes array into a final result. votes: ['hello', 'world', 'hello', 4] = 'hello'.
Ties are arbitrarily broken by being earlier in the array.


Used with modules such as:
- @tangle/strategy
- ssb-crut
