const Validator = require('is-my-json-valid')
const isEqual = require('lodash.isequal')

module.exports = function Overwrite (opts = {}) {
  const {
    identity = {},
    reifiedIdentiy = null,
    valueSchema = {
      oneOf: [
        { type: 'array' } // We want the vote to have an array of all votes cast
      ]
    }
  } = opts

  // We are looking for either a simple message or one that has a 'vote' field. If it has 'vote' then that will be an array.
  const schema = {
    oneOf: [
      {
        type: 'object',
        additionalProperties: false
      },
      {
        type: 'object',
        required: ['vote'],
        properties: {
          vote: valueSchema
        },
        additionalProperties: false
      }
    ]
  }
  const isValid = Validator(schema)

  function isIdentity (T) {
    return isEqual(T, identity)
  }

  function reify (T) {
    if (isIdentity(T)) return reifiedIdentiy

    /*
    //This turns everything into strings (js object keys are always strings!)
    //Most recent vote wins the tie
    var counts = {};
    T.vote.forEach(function(x) { counts[x] = (counts[x] || 0)+1; });  //Creates a count of everything voted on
    var result = Object.keys(counts).reduce(function(a, b){ if (counts[a] > counts[b]) return a; else return b });     //Iterate through the counts to find the one with the most votes
*/

    const counts = new Map()
    let key
    T.vote.forEach(function (x) { // Creates a count of everything voted on.
      key = JSON.stringify(x)
      counts.set(key, ((counts.get(key) || 0) + 1)) // For each x in vote, we set the count to be 1 + (the old value or 0 if that can't be found)
    })
    const result = ([...counts.entries()].reduce((a, b) => a[1] >= b[1] ? a : b)) // Spread the Map count into an array then iterate through to find the largest vote count.
    // Earlier vote wins tie
    // Bug when counting objects (maybe because it gets a reference?)
    // console.warn(counts)
    // Map(3) {
    //  { content: 'dog' } => 1,
    //  { content: 'cat' } => 1,
    //  { content: 'cat' } => 1
    // }
    return JSON.parse(result[0]) // [0] gives the thing that one, [1] gives it's vote count
  }

  function concat (a, b) {
    if (!isValid(a)) throw ConcatError(a)
    if (!isValid(b)) throw ConcatError(b)
    if (isIdentity(a)) return b // If both are identity, identity will be returned, otherwise the non-identity will be returned.
    if (isIdentity(b)) return a
    const result = (a.vote).concat(b.vote)

    return { vote: result }
  }

  function humanToTransform (currentT, change) {
    if (!isValid(currentT)) throw ConcatError(currentT)
    if (!Array.isArray(change)) throw ConcatError(change)
    if (isIdentity(currentT)) return { vote: change } // If both are identity, identity will be returned, otherwise the non-identity will be returned.
    if (isIdentity(change)) return currentT
    return { vote: (currentT.vote).concat(change) }
  }

  // function isConflict (heads) {
  //   assert(Array.isArray(heads))

  //   // try merging the heads together checking commutative at each step
  //   // e.g. if (a*b) === (b*a) then these can be merged
  //   // then check if (a*b)*c === c*(a*b) etc.
  //   // as long as set is associative with concat, then checking like this
  //   // means we are checking all possible permutations of head merging

  //   var _heads = [...heads]
  //   var a = _heads.pop()

  //   while (_heads.length) {
  //     var b = _heads.pop()

  //     if (!isEqual(concat(a, b), concat(b, a))) return true
  //     a = concat(a, b)
  //   }
  //   return false
  // }

  // function isValidMerge (heads, merge) {
  //   assert(Array.isArray(heads))

  //   if (isConflict(heads)) return !isEqual(merge, identity)
  //   // there's a conflict across the heads the merge MUST resolve

  //   return true
  //   // can apply all changes
  // }

  // function merge (heads, merge) {
  //   assert(Array.isArray(heads))
  //
  //   // TODO
  //   // is there conflict
  //   // - no: concat heads, then concat merge
  //   // - yes: is merge valid (does it resolve conflict
  //   //    - yes: apply merge
  //   //    - no: drop the merge
  //   //
  //   // TODO remember could be > 2 heads
  //
  //   return merge
  // }
  //
  // NOTE - yes i think we do want a per-strategy merge

  return {
    schema,
    isValid,
    identity: () => identity,
    concat,
    humanToTransform,
    reify
    // isConflict,
    // isValidMerge
    // merge
  }
}

function ConcatError (T) {
  return new Error(`cannot concat invalid transformation ${JSON.stringify(T)}`)
}
