const test = require('tape')
const Strategy = require('../')

const {
  isValid,
  reify,
  concat,
  identity,
  humanToTransform

  // isConflict,
  // isValidMerge
  // merge
} = Strategy()

const Ts = [
  { vote: ['hello'] },
  { vote: ['bart was here'] },
  { vote: [''] },
  { vote: [true] },
  { vote: [null] },
  { vote: [{ content: 'dog' }] },
  { vote: [5] },
  identity()
]
test('schema', t => {
  Ts.forEach(T => {
    t.true(isValid(T), `isValid : ${JSON.stringify(T)}`)
  })

  const notTs = [
    { vote: 'dog' },
    'dog',
    { content: 'dog' },
    undefined,
    { vote: undefined }
  ]
  notTs.forEach(T => {
    t.false(isValid(T), `!isValid : ${JSON.stringify(T)}`)
  })

  t.end()
})

test('reify', t => {
  Ts.forEach(T => {
    t.deepEqual(
      Ts.map(reify),
      [
        'hello',
        'bart was here',
        '',
        true,
        null,
        { content: 'dog' },
        5,
        null // default reified identity
      ],
      'reify transformation (general + identity)'
    )
  })

  t.end()
})

test('concat, identity + associativity', t => {
  t.equal(concat(identity(), Ts[0]), Ts[0], 'identiy (left)')
  t.equal(concat(Ts[0], identity()), Ts[0], 'identity (right)')

  t.equal(
    concat(
      concat(Ts[0], Ts[1]),
      Ts[2]
    ),
    concat(
      Ts[0],
      concat(Ts[1], Ts[2])
    ),
    'associativity'         //This test fails but I'm not sure why
  )

  // merging stuff! ///////////////////////////

  // const A = () => ({ set: 'dog' })
  // const B = () => ({ set: 'cat' })
  // const C = () => ({ set: 'cat-dog' }) // our merge message

  // check whether there's any conflict between n heads
  // t.equal(isConflict([A(), A()]), false, 'no conflict')
  // t.equal(isConflict([A(), identity()]), false, 'no conflict (identity)')
  // t.equal(isConflict([A(), identity(), A()]), false, 'no conflict (3-way)')

  // t.equal(isConflict([A(), B()]), true, 'isConflict')
  // t.equal(isConflict([A(), A(), B()]), true, 'isConflict (3 way)')

  // check in on which sorts of merge require a merge message, and what that looks like
  // t.equal(isValidMerge([A(), B()], C()), true, 'mege which resolves conflict is fine')
  // t.equal(isValidMerge([A(), B()], identity()), false, 'merge which does not resolve conflict invalid')
  // t.equal(isValidMerge([A(), identity()], identity()), true, 'identity merge is fine for non-conflict')

  t.end()
})

// Do humans want to enter an array or just 'dog'?
test('humanToTransform', t => {
  t.deepEqual(
    humanToTransform({}, ['dog']),
    { vote: ['dog'] }
  )
  t.deepEqual(
    humanToTransform({ vote: ['dog'] }, ['dog']),
    { vote: ['dog', 'dog'] }
  )

  t.end()
})
