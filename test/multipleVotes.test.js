const test = require('tape')
const Strategy = require('../')

const {
  isValid,
  reify,
  concat,
  identity,
  humanToTransform

  // isConflict,
  // isValidMerge
  // merge
} = Strategy()

const Ts = [
  { vote: ['hello', 'hello'] },
  { vote: ['hello', 'world'] },
  { vote: ['', '', '', '', 'a'] },
  { vote: [true, 1, 'true'] },
  { vote: [null, undefined] },
  { vote: [{ content: 'dog' }, { content: 'cat' }, { content: 'cat' }] }
  //{ vote: [{ content: 'dog' }, { content: 'cat', test: true }, { test: true, content: 'cat' }] } //Currently unsupported
]
test('valid votes', t => {
  Ts.forEach(T => {
    t.true(isValid(T), `isValid : ${JSON.stringify(T)}`)
  })
  t.end()
})

test('count votes (reify))', t => {
  t.deepEqual(
    Ts.map(reify),
    [
      'hello',
      'hello',
      '',
      true,
      null,
      { content: 'cat' }
    //  { content: 'cat', test: true }
    ],
    'reify transformation (general + identity)'
  )

  t.end()
})

test('concat votes', t => {
  t.deepEqual(concat({ vote: ['hello', 'hello'] }, { vote: ['world'] }), { vote: ['hello', 'hello', 'world'] }, 'add 1')
  t.deepEqual(concat({ vote: ['hello', '3'] }, { vote: ['world', 3] }), { vote: ['hello', '3', 'world', 3] }, 'add 2')

  t.end()
})
